# TryingAxios
<img src="https://blog.rocketseat.com.br/content/images/2020/09/axios-um-cliente-http-full-stack-rocketseat.png" />


### :rocket: Tecnologias Usadas

O projeto foi feito com as seguintes tecnologias:

- [NodeJS](https://nodejs.org/en/)
- [ReactJS](https://pt-br.reactjs.org/)

### Instalando Dependências 
```sh
yarn
npm
```
---

### Iniciando o Projeto
```sh
yarn start
npm start
```
---
### 👌 Características

- Por baixo dos panos faz requisições Ajax no browser via XMLHttpRequests;
- Faz requisições http no Node.js;
- Suporta a API de Promises;
- Intercepta requisições e respostas (request & response);
- Cancela requisições;
- Transforma os dados em JSON automaticamente;
- No lado cliente suporta a proteção contra XRSF;
- Transforma os dados da requisição e da resposta.

### :memo: Licença

Este projeto é desenvolvido sob a licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para saber mais detalhes.

<p align="center" style="margin-top: 20px; border-top: 1px solid #eee; padding-top: 20px;">Desenvolvido com :purple_heart: by <strong>Thiago dos Santos</strong> </p>
